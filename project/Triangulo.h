#include <vector>
#include "Camera.h"
class Triangulo{
  public:
    int i1, i2, i3;
    Ponto p1;
    Ponto p2;
    Ponto p3;
    Ponto p1T;
    Ponto p2T;
    Ponto p3T;
    Vetor normal;
    std::vector<Ponto> pontos;
    Triangulo(int i1, int i2, int i3){
      this->i1=i1; this->i2=i2;this->i3=i3;
    }
    Triangulo(){};
    void printPontos3D(){
      printf("%f %f %f\n", this->p1.x,this->p1.y,this->p1.z);
      printf("%f %f %f\n", this->p2.x,this->p2.y,this->p2.z);
      printf("%f %f %f\n", this->p3.x,this->p3.y,this->p3.z);
    }
    void printNormal(){
      normal.printVetor();
    }
    void printPontos2D(){
      printf("%f %f \n", this->p1T.x,this->p1T.y);
      printf("%f %f \n", this->p2T.x,this->p2T.y);
      printf("%f %f \n", this->p3T.x,this->p3T.y);
    }

    void triangleToScreenCoord(Camera C,double resolucaoX,double resolucaoY){
      this->p1T=C.pointToScreenCoord(this->p1,resolucaoX,resolucaoY);
      this->p2T=C.pointToScreenCoord(this->p2,resolucaoX,resolucaoY);
      this->p3T=C.pointToScreenCoord(this->p3,resolucaoX,resolucaoY);
    }

    void triangleVToScreenCoord(Camera C,double resolucaoX,double resolucaoY){
      this->p1T=C.pointVToScreenCoord(this->p1,resolucaoX,resolucaoY);
      this->p2T=C.pointVToScreenCoord(this->p2,resolucaoX,resolucaoY);
      this->p3T=C.pointVToScreenCoord(this->p3,resolucaoX,resolucaoY);
    }

    void fillBottomFlatTriangle(Ponto v1, Ponto v2, Ponto v3)
    {

      double invslope1 = (double)(v2.x - v1.x) / (v2.y - v1.y);
      double invslope2 = (double)(v3.x - v1.x) / (v3.y - v1.y);

      double curx1 = v1.x;
      double curx2 = v1.x;
      v1.printPonto2D();
      v2.printPonto2D();
      for (int scanlineY = v1.y; scanlineY <= v2.y; scanlineY++)
      {
        double x1 = curx1;
        double x2 = curx2;
        if (x2<x1) std::swap(x1,x2);
        for(int i = x1; i<x2; i++){
          this->pontos.push_back(Ponto(i,scanlineY));
        }
        curx1 += invslope1;
        curx2 += invslope2;
      }
    }

    void fillTopFlatTriangle(Ponto v1, Ponto v2, Ponto v3)
    {
      double invslope1 = (double)(v3.x - v1.x) / (v3.y - v1.y);
      double invslope2 = (double)(v3.x - v2.x) / (v3.y - v2.y);

      double curx1 = v3.x;
      double curx2 = v3.x;
      v1.printPonto2D();
      v3.printPonto2D();
      for (int scanlineY = v3.y; scanlineY > v1.y; scanlineY--)
      {
        double x1 = curx1;
        double x2 = curx2;
        if (x2<x1) std::swap(x1,x2);
        for(int i = x1; i<x2; i++){
          this->pontos.push_back(Ponto(i,scanlineY));
        }
        this->pontos.push_back(Ponto(curx1,scanlineY));
        this->pontos.push_back(Ponto(curx2,scanlineY));
        curx1 -= invslope1;
        curx2 -= invslope2;
      }
    }

    void drawTriangle()
    {
       /* at first sort the three vertices by y-coordinate ascending so v1 is the topmost vertice */
      sortVerticesAscendingByY();
      Ponto v1 = this->p1T;
      Ponto v2 = this->p2T;
      Ponto v3 = this->p3T;
      /* here we know that v1.y <= v2.y <= v3.y */
      /* check for trivial case of bottom-flat triangle */
      if (v2.y == v3.y)
      {
        fillBottomFlatTriangle(this->p1T, this->p2T, this->p3T);
      }
      /* check for trivial case of top-flat triangle */
      else if (v1.y == v2.y)
      {
        fillTopFlatTriangle(this->p1T, this->p2T, this->p3T);
      }
      else
      {
        /* general case - split the triangle in a topflat and bottom-flat one */
        Ponto v4 = Ponto((int)(v1.x + ((double)(v2.y - v1.y) / (double)(v3.y - v1.y)) * (v3.x - v1.x)), v2.y);
        fillBottomFlatTriangle(v1, v2, v4);
        fillTopFlatTriangle(v2, v4, v3);
      }
    }

    void sortVerticesAscendingByY(){
      Ponto a,b,c;
      if((this->p1T.y >= this->p2T.y) && (this->p1T.y >= this->p3T.y)){
        a = this->p1T;
        if(this->p2T.y >= this->p3T.y){
          b = this->p2T;
          c = this->p3T;
        }else{
          b = this->p3T;
          c = this->p2T;
        }
      }
      if((this->p2T.y >= this->p1T.y) && (this->p2T.y >= this->p3T.y)){
        a = this->p2T;
        if(this->p1T.y >= this->p3T.y){
          b = this->p1T;
          c = this->p3T;
        }else{
          b = this->p3T;
          c = this->p1T;
        }
      }
      if((this->p3T.y >= this->p2T.y) && (this->p3T.y >= this->p1T.y)){
        a = this->p3T;
        if(this->p2T.y >= this->p1T.y){
          b = this->p2T;
          c = this->p1T;
        }else{
          b = this->p1T;
          c = this->p2T;
        }
      }
      this->p1T = c;
      this->p2T = b;
      this->p3T = a;
    }
};
