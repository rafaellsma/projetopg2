#include <math.h>
#include <stdio.h>
class Vetor {
  public:
    double x;
    double y;
    double z;

    Vetor(double x, double y, double z){this->x=x;this->y=y;this->z=z;}
    Vetor(){this->x=0;this->y=0;this->z=0;};
    void normalizar(){
      double normaV = sqrt(((this->x)*(this->x)) + ((this->y)*(this->y)) + ((this->z)*(this->z)));
      this->x=((this->x)/normaV);
      this->y=((this->y)/normaV);
      this->z=((this->z)/normaV);
    }
    void corrigir(Vetor u){
      //vai usar a projecao para corrigir o vetor
      double vn = ((this->x*u.x)+(this->y*u.y) + (this->z*u.z));
      double n2 = ((u.x*u.x)+(u.y*u.y) + (u.z*u.z));
      double k = vn/n2;
      this->x=(this->x-(k*(u.x)));
      this->y=(this->y-(k*(u.y)));
      this->z=(this->z-(k*(u.z)));
    }
    void printVetor(){
      printf("%f %f %f\n",this->x,this->y,this->z);
    }
    Vetor prodVetorial(Vetor n){
      Vetor U;
      U.x=(((n.y)*(this->z))-((n.z)*(this->y)));
      U.y=(((n.z)*(this->x))-((n.x)*(this->z)));
      U.z=(((n.x)*(this->y))-((n.y)*(this->x)));
      return U;
    }
    void somar(Vetor u){
      this->x += u.x;
      this->y += u.y;
      this->z += u.z;
    }
    float escalar(Vetor u){
      return this->x*u.x + this->y*u.y + this->z*u.z;
    }
    void multEscalar(double k){
      this->x *= k;
      this->y *= k;
      this->z *= k;
    }
};
