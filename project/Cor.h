#include <stdio.h>
class Cor{
  public:
    double r;
    double g;
    double b;
    void printCor(){
      printf("%lf %lf %lf\n",r ,g ,b);
    }
    Cor(double r, double g, double b){
      this->r = r;
      this->g = g;
      this->b = b;
    }
    Cor(){
      Cor(0,0,0);
    }
};
