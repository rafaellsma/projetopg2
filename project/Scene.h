#include "Util.h"
#include <limits>
#include <algorithm>
class Scene{
  public:
    Camera camera;
    std::vector<Objeto> objetos;
    double zbuffer[2*500*500];
    double d,hx,hy;
    Iluminacao iluminacao;
    void setZbuffer(int resX, int resY){
      for (int i = 0; i < 2*500*500; i++) {
        zbuffer[i] = INT_MAX;
      }
    }

    void setCamera(){
      camera = Util::getCamera();
    }

    void setIluminacao(){
      iluminacao = Util::getIluminacao();
    }

    void setObjetos(){
      objetos = Util::getObjetos();
    }

    Cor ambiental(double ka){
      Cor c = Cor(ka*iluminacao.ia.r, ka*iluminacao.ia.g, ka*iluminacao.ia.b);
      return c;
    }

    Cor difusa(double kd, Vetor l, Vetor n, Vetor od, Cor luz){
      double k = kd*(n.escalar(l));
      Cor c = Cor(k*od.x*luz.r,k*od.y*luz.g,k*od.z*luz.b);
      return c;
    }

    Cor especular(double ks, Vetor r, Vetor v, double n, Cor luz){
      double k = pow(r.escalar(v),n)*ks;
      Cor c = Cor(k*luz.r,k*luz.g, k*luz.b);
      return c;
    }

    Cor phong(Triangulo t, Objeto o, Ponto p){
      if(p.x > 1000 || p.x < -1000){
        Cor();
      }
      if(p.y > 1000 || p.y < -1000){
        Cor();
      }
      Ponto a = t.p1T;
      Ponto b = t.p2T;
      Ponto c = t.p3T;
      printf("a\n");
      a.printPonto2D();
      printf("b\n");
      a.printPonto2D();
      printf("c\n");
      a.printPonto2D();
      std::vector<double> abg = getAlfaBetaGama(p,a,b,c);
      printf("alfa %lf beta %lf gama %lf\n",abg[0],abg[1],abg[2]);

      Ponto p1V = camera.pointToCoordVista(t.p1);
      Ponto p2V = camera.pointToCoordVista(t.p2);
      Ponto p3V = camera.pointToCoordVista(t.p3);
      printf("passou pv\n");
      Ponto p1Va = Ponto(p1V.x*abg[0],p1V.y*abg[0],p1V.z*abg[0]);
      Ponto p2Vb = Ponto(p2V.x*abg[1],p2V.y*abg[1],p2V.z*abg[1]);
      Ponto p3Vg = Ponto(p3V.x*abg[2],p3V.y*abg[2],p3V.z*abg[2]);
      printf("passou *\n");
      Ponto plinha = Ponto(p1Va.x+p2Vb.x+p3Vg.x,p1Va.y+p2Vb.y+p3Vg.y,p1Va.z+p2Vb.z+p3Vg.z);
      printf("passou plinha\n");
      int x = p.x;
      int y = p.y;
      printf("%d %d\n",x,y);
      if (plinha.z < this->zbuffer[(y*1000)+x]){
        printf("entrou if pv\n");
        this->zbuffer[(y*1000)+x] = plinha.z;
        Vetor n1V = o.pontos[t.i1].normal;
        Vetor n2V = o.pontos[t.i2].normal;
        Vetor n3V = o.pontos[t.i3].normal;
        n1V.multEscalar(abg[0]);
        n2V.multEscalar(abg[1]);
        n3V.multEscalar(abg[2]);
        Vetor n = Vetor(n1V.x + n2V.x + n3V.x,n1V.y + n2V.y + n3V.y,n1V.z + n2V.z + n3V.z);
        Vetor v = Vetor(plinha.x*-1, plinha.y*-1, plinha.z*-1);
        Vetor l = Vetor(iluminacao.l.x - plinha.x,iluminacao.l.y - plinha.y, iluminacao.l.z - plinha.z);
        n.normalizar();
        v.normalizar();
        l.normalizar();
        double nl = n.escalar(l);
        Vetor n2l = Vetor(n.x*2*nl,n.y*2*nl,n.z*2*nl);
        Vetor r = Vetor(n2l.x - l.x,n2l.y - l.y,n2l.z - l.z);
        if(n.escalar(v) < 0){
          n = Vetor(n.x*-1,n.y*-1,n.z*-1);
        }
        Cor ca = ambiental(o.ka);
        Cor d,s;
        if(nl >= 0){
          d = difusa(o.kd, l, n, o.od, iluminacao.il);
          if(r.escalar(v)>=0){
            s = especular(o.ks,r,v,o.n, iluminacao.il);
          }
        }
        Cor cor = Cor(fmin(ca.r + d.r + s.r,255), fmin(ca.g + d.g + s.g,255), fmin(ca.b + d.b + s.b,255));
        return cor;
      }
    }
    double determinante(double m[3][3]){
	    double positive = (m[0][0]*m[1][1]*m[2][2])+(m[0][1]*m[1][2]*m[2][0])+(m[0][2]*m[1][0]*m[2][1]);
	    double negative = (m[2][0]*m[1][1]*m[0][2])+(m[2][1]*m[1][2]*m[0][0])+(m[2][2]*m[1][0]*m[0][1]);
      return positive-negative;
    }
    std::vector<double> getAlfaBetaGama(Ponto ponto, Ponto a, Ponto b, Ponto c){
        Ponto p1T = a;
        Ponto p2T = b;
        Ponto p3T = c;
	      double detA;
	      double A [3][3];
	      std::vector<double>resp;
	      A[0][0]=p1T.x;A[0][1]=p2T.x;A[0][2]=p3T.x;
	      A[1][0]=p1T.y;A[1][1]=p2T.y;A[1][2]=p3T.y;
	      A[2][0] = 1;  A[2][1] = 1;  A[2][2] = 1;
	      detA=determinante(A);
	      double aux[3][3];
	      for(int i=0;i<3;i++){
	        for(int j=0;j<3;j++){
	          aux[i][j]=A[i][j];
	        }
	      }
	      //para x:
	      aux[0][0]=ponto.x;
	      aux[1][0]=ponto.y;
	      resp.push_back(determinante(aux)/detA);
	      aux[0][0]=p1T.x;
	      aux[1][0]=p1T.y;
	      //para y:
	      aux[0][1]=ponto.x;
	      aux[1][1]=ponto.y;
	      resp.push_back(determinante(aux)/detA);
	      aux[0][1]=p2T.x;
	      aux[1][1]=p2T.y;
	      //para z:
	      aux[0][2]=ponto.x;
	      aux[1][2]=ponto.y;
	      resp.push_back(determinante(aux)/detA);
	      aux[0][2]=p3T.x;
        aux[1][2]=p3T.y;
	      return resp;
	    }
};
