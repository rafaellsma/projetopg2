#include "Objeto.h"

class Util{
  public:
    static Camera getCamera(){
      double cx, cy, cz;
      double nx, ny, nz;
      double vx, vy, vz;
      double d, hx ,hy;
      FILE * fp;
      fp = fopen ("camera.cfg", "r");
      fscanf(fp, "%lf %lf %lf", &cx, &cy, &cz);
      fscanf(fp, "%lf %lf %lf", &nx, &ny, &nz);
      fscanf(fp, "%lf %lf %lf", &vx, &vy, &vz);
      fscanf(fp, "%lf %lf %lf", &d, &hx, &hy);
      Ponto c = Ponto(cx,cy,cz);
      Vetor n = Vetor(nx,ny,nz);
      Vetor v = Vetor(vx,vy,vz);
      Camera camera = Camera(c, n, v, d, hx, hy);
      fclose(fp);
      return camera;
    }

    static Iluminacao getIluminacao(){
      Ponto l;
      Cor ia;
      Cor il;
      FILE * fp;
      fp = fopen ("Iluminacao.txt", "r");
      fscanf(fp, "%lf %lf %lf", &l.x, &l.y, &l.z);
      fscanf(fp, "%lf %lf %lf", &ia.r, &ia.g, &ia.b);
      fscanf(fp, "%lf %lf %lf", &il.r, &il.g, &il.b);
      Iluminacao i = Iluminacao(l,ia,il);
      fclose(fp);
      return i;
    }

    static std::vector<Objeto> getObjetos(){
      std::vector<Objeto> objetos;
      int qO = 0, qP=0, qT=0;
      FILE * fp;
      fp = fopen ("Objetos.byu", "r");
      fscanf(fp, "%d", &qO);
      for(int i = 0; i<qO; i++){
        Objeto o = Objeto();
        fscanf(fp, "%d %d", &qP, &qT);
        for(int j = 0; j<qP; j++){
          double px,py,pz;
          fscanf(fp, "%lf %lf %lf", &px, &py, &pz);
          o.pontos.push_back(Ponto(px,py,pz));
        }
        for(int j = 0; j<qT; j++){
          double v1,v2,v3;
          fscanf(fp, "%lf %lf %lf", &v1, &v2, &v3);

          Triangulo t = Triangulo(v1-1,v2-1,v3-1);
          t.p1 = o.pontos[t.i1];
          t.p2 = o.pontos[t.i2];
          t.p3 = o.pontos[t.i3];
          o.triangulos.push_back(t);

        }
        fscanf(fp, "%lf %lf %lf", &o.ka, &o.kd, &o.ks);
        fscanf(fp, "%lf", &o.n);
        fscanf(fp, "%lf %lf %lf", &o.od.x, &o.od.y, &o.od.z);
        fclose(fp);
        objetos.push_back(o);
      }
      return objetos;
    }
};
