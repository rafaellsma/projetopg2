#include <vector>
#include "Triangulo.h"

class Objeto{
  public:
      std::vector<Ponto> pontos;
      std::vector<Triangulo> triangulos;
      double ka, kd, ks, n;
      Vetor od;

      Objeto(double ka, double kd, double ks, double n, Vetor color) {
        	this->ka = ka;
        	this->kd = kd;
        	this->ks = ks;
        	this->n = n;
        	this->od = color;
      }
      Objeto(){

      }

      void transformTodosPontoEmVista(Camera c){
        for (int j = 0; j < this->pontos.size(); j++) {
          this->pontos[j] = c.pointToCoordVista(this->pontos[j]);
        }
      }
      Vetor calculeNormal(Ponto p1, Ponto p2, Ponto p3){
       Vetor ab = Vetor(p2.x - p1.x, p2.y - p1.y, p2.z - p1.z);
       Vetor ac = Vetor(p3.x - p1.x, p3.y - p1.y, p3.z - p1.z);
       Vetor normal = ac.prodVetorial(ab);
       normal.normalizar();
       return normal;
      }
      void normalizarNormais() {
        for (int i = 0; i < this->pontos.size(); i++) {
          this->pontos[i].normal.normalizar();
        }
      }
      void calcularNormais(){
        for (int i = 0; i < this->triangulos.size(); i++) {
          Ponto a = this->pontos[this->triangulos[i].i1];
          Ponto b = this->pontos[this->triangulos[i].i2];
          Ponto c = this->pontos[this->triangulos[i].i3];
          this->triangulos[i].normal = calculeNormal(a,b,c);
          this->pontos[this->triangulos[i].i1].normal.somar(this->triangulos[i].normal);
          this->pontos[this->triangulos[i].i2].normal.somar(this->triangulos[i].normal);
          this->pontos[this->triangulos[i].i3].normal.somar(this->triangulos[i].normal);
        }

        normalizarNormais();

      }
      void gerarVertices(Camera cam, double resX, double resY){
        for (int i = 0; i < this->triangulos.size(); i++) {
          this->triangulos[i].triangleToScreenCoord(cam,resX,resY);
        }
      }
      void printObjeto(){
          for(int i = 0; i<this->pontos.size(); i++){
            this->pontos[i].printPonto3D();
          }
          for(int i = 0; i<this->triangulos.size(); i++){
            printf("Triangulo %d\n",i+1);
            printf("pontos 3D vista\n" );
            this->triangulos[i].printPontos3D();
            printf("pontos 2D vista\n");
            this->triangulos[i].printPontos2D();
            printf("normal");
            this->triangulos[i].printNormal();
          }
          printf("ka: %lf kd: %lf ks:%lf n:%lf\n", this->ka,this->kd,this->ks,this->n);
          this->od.printVetor();
      }
};
