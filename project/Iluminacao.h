#include "Ponto.h"
class Iluminacao{
  public:
    Ponto l;
    Cor ia;
    Cor il;

    Iluminacao(Ponto l, Cor ia, Cor il){
      this->l = l;
      this->ia = ia;
      this->il = il;
    }
    Iluminacao(){
      
    }
    void printIluminacao(){
      this->l.printPonto3D();
      this->ia.printCor();
      this->il.printCor();
    }
};
