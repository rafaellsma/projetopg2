#include "Cor.h"
#include <stdlib.h>
class Ponto{
  public:
    double x;
    double y;
    double z;
    Vetor normal;
    Cor c;

    Ponto(double x, double y, double z){
        this->x=x;
        this->y=y;
        this->z=z;
        normal = Vetor(0,0,0);
    }

    Ponto(double x, double y){
        this->x=x;
        this->y=y;
        normal = Vetor(0,0,0);
    }

    Ponto(){
        this->x=0;
        this->y=0;
        this->z=0;
        normal = Vetor(0,0,0);
    }
    void setNormal(double x, double y, double z){
      this->normal.x = x;
      this->normal.y = y;
      this->normal.z = z;
      this->normal.normalizar();
    }
    void printPonto3D(){
      printf("%f %f %f\n",this->x,this->y,this->z);
      normal.printVetor();
    }
    void printPonto2D(){
      printf("%f %f\n",this->x,this->y);
    }
};
