#include <GLFW/glfw3.h>
#include "Scene.h"

using namespace std;
//eventos
void mbpressed(GLFWwindow* window, int button, int action, int mods){
}
void mmove(GLFWwindow* window, double xpos, double ypos){
}

int main(void){
    GLFWwindow* window;
    if (!glfwInit())return -1;
    window = glfwCreateWindow(1000, 1000,  "MultiploObjetos", NULL, NULL);
    if (!window){
        glfwTerminate();
        return -1;
    }
    glfwMakeContextCurrent(window);
    glfwSetMouseButtonCallback(window, mbpressed);
    glfwSetCursorPosCallback(window, mmove);
    std::vector<Cor> cores;
    cores.push_back(Cor(1,0,0));
    cores.push_back(Cor(0,1,0));
    cores.push_back(Cor(0,0,1));
    cores.push_back(Cor(1,0,1));
    int ic;
    while (!glfwWindowShouldClose(window)){
        glClearColor(0, 0 , 0 , 1.0f);
        glClear(GL_COLOR_BUFFER_BIT);
        ic = 0;
        Scene cena;
        cena.setCamera();
        cena.setIluminacao();
        cena.setObjetos();
        cena.iluminacao.l = cena.camera.pointToCoordVista(cena.iluminacao.l);
        for (int i = 0; i < cena.objetos.size(); i++) {
            cena.objetos[i].printObjeto();
        }
        for (int i = 0; i < cena.objetos.size(); i++) {
            cena.objetos[i].transformTodosPontoEmVista(cena.camera);
        }
        for (int i = 0; i < cena.objetos.size(); i++) {
            cena.objetos[i].calcularNormais();
        }
        for (int i = 0; i < cena.objetos.size(); i++) {
            cena.objetos[i].gerarVertices(cena.camera, 1000,1000);
        }
        for (int i = 0; i < cena.objetos.size(); i++) {
            for (int j = 0; j < cena.objetos[i].triangulos.size(); j++) {
              cena.objetos[i].triangulos[j].drawTriangle();
            }
        }
        printf("passou\n");
        cena.setZbuffer(1000,1000);

        glBegin(GL_POINTS);
        for (int i = 0; i < cena.objetos.size(); i++) {
            for (int j = 0; j < cena.objetos[i].triangulos.size(); j++) {
                ic = j%4;
                glColor3f(cores[ic].r,cores[ic].g,cores[ic].b);
              for (int k = 0; k < cena.objetos[i].triangulos[j].pontos.size(); k++) {
                glVertex2f(cena.objetos[i].triangulos[j].pontos[k].x/1000,cena.objetos[i].triangulos[j].pontos[k].y/1000);
              }
            }
        }
        glEnd();

	      glfwSwapBuffers(window);
        glfwPollEvents();
    }
    glfwTerminate();
    return 0;
}
