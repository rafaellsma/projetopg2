#include "Vetor.h"
#include "Iluminacao.h"
class Camera{
  public:
    Ponto c;
    Vetor n,v;
    double d,hx,hy;
    Camera(Ponto c,Vetor n,Vetor v,double d,double hx,double hy){
      this->c=c;
      this->n=n;
      this->v=v;
      this->d=d;
      this->hx=hx;
      this->hy=hy;
    }
    Camera(){

    }
    void printCamera(){
      c.printPonto3D();
      n.printVetor();
      v.printVetor();
      printf("%lf %lf %lf\n",d, hx, hy );
    }
    Ponto pointToScreenCoord(Ponto P,double resolucaoX,double resolucaoY){
      Ponto P1Linha = obterPLinha(pointToCoordVista(P));
      //calcula P em coordenada de tela
      Ponto P1Tela = obterPTela(P1Linha,resolucaoX,resolucaoY);
      return P1Tela;
    }
    Ponto pointVToScreenCoord(Ponto P,double resolucaoX,double resolucaoY){
      //calcula P em coordenada de tela
      Ponto P1Tela = obterPTela(P,resolucaoX,resolucaoY);
      return P1Tela;
    }
    Ponto obterPLinha(Ponto pontoAux){
      Ponto resp;
      resp.x=((pontoAux.x/pontoAux.z) * (this->d/this->hx));
      resp.y=((pontoAux.y/pontoAux.z) * (this->d/this->hy));
      return resp;
    }

    Ponto obterPTela(Ponto p,double resX,double resY){
      Ponto resp;
      resp.x=(((p.x+1)/2)*resX);
      resp.y=(((1-p.y)/2)*resY);
      return resp;
    }
    Ponto pointToCoordVista(Ponto P){
      Ponto PV;
      //corrige e normaliza os vetores necessario. Calcula o vetor U
      this->v.corrigir(this->n);
      this->v.normalizar();
      this->n.normalizar();
      Vetor U = this->v.prodVetorial(this->n);

      //faz a matriz mudanca de base
      double matrix [3][3];
      double *auxM[3]={matrix[0],matrix[1],matrix[2]};
      matrizMudancaBase(U,this->v,this->n,auxM);
      //faz as matrizes de P-C
      double MP1C [3][1];
      fillMatrix(MP1C,P,this->c);
      //matriz resultante da multiplciacao de I * [P-C]
      double MP1V[3][1];
      double *auxMatrix[3] = { MP1V[0], MP1V[1], MP1V[2]};
      multiplicar(matrix,MP1C,auxMatrix);
      //converte as matrizes acima para pontos
      Ponto pontoV1=matrixToPoint(MP1V);
      //pontoV1.printPonto3D();
      return pontoV1;
    }
    void multiplicar(double a[3][3], double b[3][1], double ** resp){
      double sum =0;
      for (int i = 0; i < 3; i++) {
        for (int j = 0; j < 1; j++) {
          for (int k = 0; k < 3; k++) {
            sum = sum + a[i][k]*b[k][j];
          }
          resp[i][j]=sum;
          sum = 0;
        }
      }
    }
    //funcao para obter a matriz de mudanca de base da base canonica (coordenadas de mundo)
    //para a base alfa (coordenadas de vista):
    void matrizMudancaBase(Vetor u,Vetor v,Vetor n, double ** matrix){
      for(int i=0;i<3;i++){
        if(i==0){
          matrix[i][0]=u.x;
          matrix[i][1]=u.y;
          matrix[i][2]=u.z;
        }
        else if(i==1){
          matrix[i][0]=v.x;
          matrix[i][1]=v.y;
          matrix[i][2]=v.z;
        }
        else{
          matrix[i][0]=n.x;
          matrix[i][1]=n.y;
          matrix[i][2]=n.z;
        }
      }
    }
    //preencher uma matrix com a subtração de pontos
    void fillMatrix(double m[3][1],Ponto P, Ponto C){
      m[0][0]=P.x-C.x;
      m[1][0]=P.y-C.y;
      m[2][0]=P.z-C.z;
    }
    //converter uma matrix que representa um ponto em um ponto
    Ponto matrixToPoint(double m[3][1]){
      Ponto resp;
      resp.x=(m[0][0]);
      resp.y=(m[1][0]);
      resp.z=(m[2][0]);
      return resp;
    }
};
